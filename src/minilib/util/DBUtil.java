package minilib.util;
import java.sql.*;

public class DBUtil {
	private static String rootname="root";  	
	private static String rootpass="123456";
	private static String driver="com.mysql.cj.jdbc.Driver";
	//private static String url="jdbc:mysql://localhost:3306/book";
     private static String url="jdbc:mysql://localhost:3306/book?useSSL=true&serverTimezone=GMT";
	public static Connection getConnection()throws InstantiationException,IllegalAccessException,ClassNotFoundException,SQLException{
		Class.forName(driver).newInstance();
		return DriverManager.getConnection(url, rootname, rootpass);
	}
}